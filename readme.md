# Survey App

## Setup
### Docker
These instructions have been tested on Ubuntu Linux and MacOS 10.14 (with Docker Desktop).

If you use Windows, then please note the [system requirements](https://docs.docker.com/docker-for-windows/install/). If your version falls outside of this, then rather use Valet, Homestead, Vagrant, etc.

1. Stop local web server(s) and ensure that ports 80 and 3306 are usable by Docker
1. [Install Docker](https://docs.docker.com/install/)
1. Clone the repository to your local and change directory to the newly created directory
1. Execute `docker run --rm -v $(pwd):/app composer:1.8.6 install` where "$(pwd) is the cloned directory"
1. Execute `docker-compose up --build` to pull the necessary Docker images and start the project containers
1. Execute `docker container exec -it zthapp_web_1 /bin/bash` to access the web container's bash. While in the bash, execute the following:
    1. `npm install`
    1. `php artisan migrate`
    1. `exit`
1. Go to [http://localhost/](http://localhost/)

### Vagrant, Homestead, Valet, etc
The project uses MySQL as the database server and `zthapp` as the database. Create the database first, before executing the artisan migration command.

## Usage
Users have to register, confirm their email addresses and login before they are able to administrate Surveys

### Creating a Survey
1. Log in
1. Click on the Survey Administration link in the left pane
1. Click on the Create Survey button in the top right
1. Fill out the form with:
    1. Name - which will be used to identify surveys
    1. Question
    1. Expiry date and time - when this time is reached, then the survey will be flagged as completed and users won't be able to vote on it anymore
    1. Question Options - these will form the possible answers a user can choose from
1. Save - on success the Survey will be saved and the user will see the data in an edit form

### Editing 
1. Log in
1. Click on the Survey Administration link in the left pane
1. Click on the Edit button next to the Survey to be edited
1. An edit form will be displayed where the data can be changed
1. Save - on success, the Survey data will be saved and a success message will be displayed

### Deleting
1. Log in
1. Click on the Survey Administration link in the left pane
1. Click the Delete button next to the Survey to be deleted
1. A popup modal will be displayed with a confirmation message, click the Delete button to delete the Survey, or the Cancel button to close the modal popup

Alternatively, follow steps 1 and 2 above, then
1. Click on the Edit button next to the Survey to be deleted
1. Click the Delete button at the bottom of the edit page
1. A popup modal will be displayed with a confirmation message, click the Delete button to delete the Survey, or the Cancel button to close the modal popup

### Sharing
A Share button is displayed below the Save button on the edit page (see the steps for Editing above)

When the button is clicked, a popup modal is displayed where a comma separated email list can be entered and sent (beware with using too many email addresses with mailtrap.io as it will cause rate limit exceptions)

### Completing Expired Surveys
The `survey:complete` artisan command has been set up as a scheduled task as part of `schedule:run`. Every time it is executed, expired surveys will be flagged as completed, they will display the vote results and users won't be able to vote on them anymore.

Please ensure that `schedule:run` has an entry in the crontab for the service to execute.

### Voting
A user has to access the "show" page of a Survey in order to cast a vote. This can be done by either accessing the Surveys from the home page or by using the links from the Survey share emails.

Once a user votes on a survey, then a browser cookie is set and the vote is counted. 
