<?php

use \Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Route;
use \App\Http\Controllers\SurveyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('home'));
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('survey', 'SurveyController')->middleware('verified');
Route::get('/survey/{survey}', [SurveyController::class, 'show'])
    ->name('survey.show');
Route::post('/survey/vote/{survey}', [SurveyController::class, 'vote'])
    ->name('survey.vote');
Route::post('/survey/share/{survey}', [SurveyController::class, 'share'])
    ->name('survey.share');
