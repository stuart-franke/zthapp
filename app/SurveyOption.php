<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SurveyOption
 * @package App
 */
class SurveyOption extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['option_text', 'survey_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function surveyvote()
    {
        return $this->hasMany(SurveyVote::class);
    }
}
