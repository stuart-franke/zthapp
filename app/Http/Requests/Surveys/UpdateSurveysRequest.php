<?php

namespace App\Http\Requests\Surveys;

use App\Survey;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateSurveysRequest
 * @package App\Http\Requests\Surveys
 */
class UpdateSurveysRequest extends FormRequest
{
    /**
     * @var \DateTime
     */
    protected $today;

    /**
     * UpdateSurveysRequest constructor.
     * @param array $query
     * @param array $request
     * @param array $attributes
     * @param array $cookies
     * @param array $files
     * @param array $server
     * @param null $content
     */
    public function __construct(
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    )
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);

        $this->today = \DateTime::createFromFormat(Survey::DATETIME_FORMAT, date(Survey::DATETIME_FORMAT))
            ->format(Survey::DATETIME_FORMAT);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $survey = Survey::find($this->route('survey'));

        return $survey && (int)$survey->first()->user_id === (int)$this->user()->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'question' => 'required|string',
            'expires_at' => 'nullable|dateformat:' . Survey::DATETIME_FORMAT . '|after_or_equal:' . $this->today,
            'option' => 'required|array',
            'option.*' => 'required|string|max:255',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'The name field is required',
            'question.required' => 'The question field is required',
            'expires_at.dateformat' => 'The expiry date format has to be YYYY-MM-DD HH:MM',
            'expires_at.after_or_equal' => 'The expiry date must be after or on ' . $this->today,
            'option.required' => 'The question options are required',
            'option.*.required' => 'The question options are required',
        ];
    }
}
