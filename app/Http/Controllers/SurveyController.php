<?php

namespace App\Http\Controllers;

use App\Http\Requests\Surveys\CreateSurveysRequest;
use App\Http\Requests\Surveys\UpdateSurveysRequest;
use App\Survey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SurveyShare;

/**
 * Class SurveyController
 * @package App\Http\Controllers
 */
class SurveyController extends Controller
{
    /**
     * SurveyController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['show', 'vote']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('survey.index')->with('surveys', Survey::where('user_id', auth()->user()->id)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('survey.create')->with('request', $request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateSurveysRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(CreateSurveysRequest $request)
    {
        try {
            /** @var Survey $survey */
            $survey = Survey::create([
                'name' => $request->name,
                'question' => $request->question,
                'completed' => ($this->getCompleted($request->expires_at) === -1 ? 1 : 0),
                'expires_at' => $request->expires_at,
                'user_id' => auth()->user()->id,
            ]);
            $survey->syncSurveyOptions($request->option);
        } catch (\Exception $exception) {
            session()->flash('error', __('An error occurred while creating the Survey. Please try again.'));

            return redirect(route('survey.create'));
        }

        session()->flash('success', __('Survey created successfully'));

        return redirect(route('survey.edit', $survey->id));
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param Survey $survey
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, Survey $survey)
    {
        $cookieIsSet = ($request->cookie($this->getCookieName((int)$survey->id)) ? true : false);
        $votes = $this->getVoteCounts((int)$survey->id);

        return view('survey.show')
            ->with('survey', $survey)
            ->with('cookieIsSet', $cookieIsSet)
            ->with('votes', $votes);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Survey $survey
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit(Survey $survey)
    {
        if ((int)auth()->user()->id !== (int)$survey->user->id) {
            throw new \Exception('Unauthorised action');
        }

        return view('survey.create')->with('survey', $survey);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSurveysRequest $request
     * @param Survey $survey
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function update(UpdateSurveysRequest $request, Survey $survey)
    {
        $error = false;
        $message = 'Survey updated successfully';

        try {
            $survey->update([
                'name' => $request->name,
                'question' => $request->question,
                'completed' => ($this->getCompleted($request->expires_at) === -1 ? 1 : 0),
                'expires_at' => $request->expires_at,
            ]);
            $survey->syncSurveyOptions($request->option);
        } catch (\Exception $exception) {
            $error = true;
            $message = 'An error occurred while updating the Survey. Please try again.';
        }

        session()->flash(((bool)$error === true ? 'error' : 'success'), __($message));

        return redirect(route('survey.edit', $survey->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Survey $survey
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Survey $survey)
    {
        if ((int)auth()->user()->id !== (int)$survey->user->id) {
            throw new \Exception('Unauthorised action');
        }

        $survey->delete();

        session()->flash('success', __('Survey deleted successfully'));

        return redirect(route('survey.index'));
    }

    /**
     * @param Request $request
     * @param Survey $survey
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function vote(Request $request, Survey $survey)
    {
        $existingCookie = $request->cookie($this->getCookieName((int)$survey->id));

        if ($existingCookie) {
            session()->flash('success', __('You have already voted for this survey'));

            return redirect(route('survey.show', $survey->id))->cookie($existingCookie);
        }

        $surveyOptionId = (int)$request->option;
        $cookie = cookie(
            $this->getCookieName((int)$survey->id),
            Hash::make($survey->id . '-' . $surveyOptionId),
            60 * 24 * 365
        );

        $survey->surveyvote()->create([
            'survey_id' => (int)$survey->id,
            'survey_option_id' => $surveyOptionId,
            'cookie' => $cookie->getValue(),
        ]);

        session()->flash('success', __('You have voted successfully'));

        return redirect(route('survey.show', $survey->id))->cookie($cookie);
    }

    /**
     * @param Request $request
     * @param Survey $survey
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function share(Request $request, Survey $survey)
    {
        try {
            $error = false;
            $message = __('Survey shared successfully');
            $emails = explode(',', trim(str_replace(' ', '', $request->emails)));
            $content = [
                'from_email' => auth()->user()->email,
                'from_name' => auth()->user()->name,
                'subject' => __('A Survey was shared with you!'),
                'body' => __(
                    sprintf(
                        '%s shared the survey %s with you, please click the button below to cast your vote.',
                        auth()->user()->name,
                        $survey->name
                    )
                ),
                'buttonText' => __('Vote Now!'),
                'buttonUrl' => route('survey.show', $survey->id)
            ];

            Mail::to(auth()->user()->email)
                ->bcc($emails)
                ->send(new SurveyShare($content));
        } catch (\Exception $exception) {
            $error = true;
            $message = __('An error occurred while sharing the Survey, please try again');
        }

        session()->flash(((bool)$error === true ? 'error' : 'success'), __($message));

        return redirect(route('survey.edit', $survey->id));
    }

    /**
     * @param string $expiresAt
     * @return int
     */
    protected function getCompleted($expiresAt = '')
    {
        if (trim($expiresAt) === '') {
            return 0;
        }

        $dateNow = \DateTime::createFromFormat(Survey::DATETIME_FORMAT, date(Survey::DATETIME_FORMAT));
        $dateExpiresAt = \DateTime::createFromFormat(Survey::DATETIME_FORMAT, $expiresAt);

        return $dateExpiresAt <=> $dateNow;
    }

    /**
     * @param int $surveyId
     * @return string
     */
    protected function getCookieName(int $surveyId)
    {
        return Survey::COOKIE_NAME . '-' . $surveyId;
    }

    /**
     * @param int $surveyId
     * @return array
     */
    public function getVoteCounts(int $surveyId)
    {
        $votes = DB::table('surveys')
            ->select(
                'survey_votes.survey_option_id',
                DB::raw('COUNT(survey_votes.survey_option_id) AS vote_count')
            )
            ->join('survey_options', 'surveys.id', '=', 'survey_options.survey_id')
            ->join('survey_votes', function ($join) {
                $join->on('surveys.id', '=', 'survey_votes.survey_id')
                    ->on('survey_options.id', '=', 'survey_votes.survey_option_id');
            })
            ->where('surveys.id', $surveyId)
            ->groupBy('survey_votes.survey_option_id')
            ->orderBy('vote_count', 'desc')
            ->orderBy('survey_votes.survey_option_id')
            ->get()
            ->toArray();

        if (count($votes) === 0) {
            return ['max_vote_count' => 0];
        }

        $voteData = [];
        $mostVotes = reset($votes);
        $voteData['max_vote_count'] = $mostVotes->vote_count;

        foreach ($votes as $vote) {
            $voteData[$vote->survey_option_id] = [
                'vote_count' => $vote->vote_count,
                'count_percentage' => ($vote->vote_count / $voteData['max_vote_count']) * 100
            ];
        }

        return $voteData;
    }
}
