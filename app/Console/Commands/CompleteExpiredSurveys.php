<?php

namespace App\Console\Commands;

use App\Survey;
use Illuminate\Console\Command;

/**
 * Class CompleteExpiredSurveys
 * @package App\Console\Commands
 */
class CompleteExpiredSurveys extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'survey:complete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Complete expired Surveys';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $expiredSurveys = Survey::where('completed', 0)
            ->whereNotNull('expires_at')
            ->whereDate('expires_at', '<=', date(Survey::DATETIME_FORMAT))
            ->get();

        try {
            foreach ($expiredSurveys as $survey) {
                $survey->update(['completed' => 1]);
                $this->info(sprintf('Survey %s expired and was flagged as completed', $survey->name));
            }
        } catch (\Exception $exception) {
            $this->error(
                sprintf(
                    '%s occurred while flagging Survey %s as completed',
                    $exception->getMessage(),
                    $survey->name
                )
            );
        }
    }
}
