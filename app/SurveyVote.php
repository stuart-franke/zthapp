<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SurveyVote
 * @package App
 */
class SurveyVote extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['survey_id', 'survey_option_id', 'cookie'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function surveyoption()
    {
        return $this->belongsTo(SurveyOption::class);
    }
}
