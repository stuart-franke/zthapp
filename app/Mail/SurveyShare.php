<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class SurveyShare
 * @package App\Mail
 */
class SurveyShare extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var array
     */
    public $content;

    /**
     * SurveyShare constructor.
     * @param array $content
     */
    public function __construct(array $content)
    {
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->content['from_email'], $this->content['from_name'])
            ->subject($this->content['subject'])
            ->markdown('survey.email.share')
            ->with('content', $this->content);
    }
}
