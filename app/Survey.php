<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Survey
 * @package App
 */
class Survey extends Model
{
    const COOKIE_NAME = 'zthasvt';
    const DATETIME_FORMAT = 'Y-m-d H:i';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'question', 'completed', 'expires_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function surveyoption()
    {
        return $this->hasMany(SurveyOption::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function surveyvote()
    {
        return $this->hasMany(SurveyVote::class);
    }

    /**
     * Custom sync functionality for the one to many relationship that'll keep the foreign key constraints intact
     *
     * @param array $surveyOptionRequest
     * @throws \Exception
     */
    public function syncSurveyOptions(array $surveyOptionRequest)
    {
        $surveyOptionData = $this->getSurveyOptions($surveyOptionRequest);
        $existingOptions = [];

        /** @var SurveyOption $existingOption */
        foreach ($this->surveyoption->all() as $existingOption) {
            $existingOptions[$existingOption->id] = $existingOption;
        }

        $optionsToBeUpdated = array_intersect_key($existingOptions, $surveyOptionData);
        $optionsToBeAdded = array_diff_key($surveyOptionData, $existingOptions);
        $optionsToBeDeleted = array_diff_key($existingOptions, $surveyOptionData);

        /** @var SurveyOption $optionToBeUpdated */
        foreach ($optionsToBeUpdated as $optionToBeUpdatedId => $optionToBeUpdated) {
            $optionToBeUpdated->update(['option_text' => $surveyOptionData[$optionToBeUpdatedId]['option_text']]);
        }

        /** @var array $optionToBeAdded */
        foreach ($optionsToBeAdded as $optionToBeAdded) {
            $this->surveyoption()->create(['survey_id' => $this->id, 'option_text' => $optionToBeAdded['option_text']]);
        }

        /** @var SurveyOption $optionToBeDeleted */
        foreach ($optionsToBeDeleted as $optionToBeDeleted) {
            $optionToBeDeleted->delete();
        }
    }

    /**
     * @param array $surveyOptions
     * @return array
     */
    protected function getSurveyOptions(array $surveyOptions)
    {
        $options = [];

        if (count($surveyOptions) > 0) {
            foreach ($surveyOptions as $id => $option) {
                $options[$id] = ['id' => $id, 'option_text' => $option];
            }
        }

        return $options;
    }
}
