@extends('layouts.app')

@section('content')
    @if($surveys->count() > 0)
        <div class="row">
            @foreach($surveys as $survey)
                <div class="col-md-4 mb-4">
                    <div class="card shadow-sm">
                        <div class="card-header h-100">
                            <h3 class="m-0 h-100">{{ $survey->name }}</h3>
                        </div>
                        <div class="card-body h-100">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <button type="button"
                                            onclick="window.location.href='{{ route('survey.show', $survey->id) }}'"
                                            class="btn btn-sm btn-outline-primary">View
                                    </button>
                                    @auth()
                                        @if((int)auth()->user()->id === (int)$survey->user_id)
                                            <button type="button"
                                                    onclick="window.location.href='{{ route('survey.edit', $survey->id) }}'"
                                                    class="btn btn-sm btn-outline-secondary">Edit
                                            </button>
                                        @endif
                                    @endauth
                                </div>
                                @if((bool)$survey->completed === true)
                                    <span class="badge badge-pill badge-danger">{{ __('Expired') }}</span>
                                @else
                                    <span class="badge badge-pill badge-success">{{ __('Active') }}</span>
                                @endif
                                <small class="text-muted">
                                    {{ $survey->surveyvote()->count() . ' ' . ((int)$survey->surveyvote()->count() === 1 ? __('vote') : __('votes')) }}
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        <div class="row">
            <div class="col-md-12 mb-4">
                <div class="card shadow-sm">
                    <div class="card-header h-100">
                        <h3 class="m-0 h-100">{{ __('Surveys') }}</h3>
                    </div>
                    <div class="card-body h-100">
                        <div class="d-flex justify-content-between align-items-center">
                            <p>{{ __('No Surveys found, please create some from the') }} <a
                                    href="{{ route('survey.index') }}">{{ __('Survey Administration') }}</a> {{ __('page.') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
