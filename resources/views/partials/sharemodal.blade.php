<div class="modal fade" id="shareModal" tabindex="-1" role="dialog"
     aria-labelledby="shareModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="" method="post" id="shareSurveyForm">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="shareModalLabel">{{ __('Share Survey') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="emails">{{ __('Email Address(es)') }}</label>
                    <small class="form-text text-muted">{{ __('Enter the email address(es) with whom the survey should be shared with. Comma separate multiple email addresses.') }}</small>
                    <input type="text" class="form-control" name="emails" id="emails" value="{{ old('emails') }}"
                           required>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-primary">{{ __('Send') }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Cancel') }}</button>
                </div>
            </div>
        </form>
    </div>
</div>
