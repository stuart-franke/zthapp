@extends('layouts.app')

@section('content')
    @php($newSurvey = isset($survey) ? false : true)
    <div class="card card-default">
        <div class="card-header">
            <h3 class="float-left align-middle m-0">
                {{ ($newSurvey === false) ? __('Edit Survey') : __('Create Survey') }}
            </h3>
        </div>
        <div class="card-body">
            @include('partials.errors')
            <form method="post"
                  action="{{ ($newSurvey === false) ? route('survey.update', $survey->id) : route('survey.store') }}"
                  id="form">
                @csrf
                @if($newSurvey === false)
                    @method('PUT')
                @endif
                <div class="form-group">
                    <label for="name">{{ __('Name') }}</label>
                    <input type="text" class="form-control" name="name" id="name"
                           value="{{ ($newSurvey === false) ? $survey->name : ($request->old('name') ? : '') }}"
                           required>
                    @error('name')
                    <span class="invalid-feedback" role="alert" style="display: block;">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="question">{{ __('Question') }}</label>
                    <textarea class="form-control" name="question" id="question" cols="30"
                              rows="10"
                              required>{{ ($newSurvey === false) ? $survey->question : ($request->old('question') ? : '') }}</textarea>
                    @error('question')
                    <span class="invalid-feedback" role="alert" style="display: block;">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="expires-at">{{ __('Expiry Date') }}</label>
                    <input type="text" class="form-control" name="expires_at" id="expires-at"
                           value="{{ ($newSurvey === false) ? $survey->expires_at  : ($request->old('expires_at') ? : '') }}">
                    @error('expires_at')
                    <span class="invalid-feedback" role="alert" style="display: block;">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <label class="form-group m-0" for="option-container">{{ __('Question Options') }}</label>
                <small
                    class="form-text text-muted">{{ __('Enter answer options to the Survey question. Click the Add Option button to add another answer option and click the Remove button next to an answer option to remove it.') }}</small>
                <div id="option-container">
                    @if($newSurvey === true)
                        @if(is_array($request->old('option')) && count($request->old('option')) > 0)
                            @foreach($request->old('option') as $id => $option)
                                <div class="form-row" id="form-row-option-{{ $id }}">
                                    <div class="form-group col-md-11">
                                        <input class="form-control option-input" type="text"
                                               name="option[{{ $id }}]" id="option-{{ $id }}"
                                               value="{{ $option }}" data-id="{{ $id }}">
                                    </div>
                                    <div class="form-group col-md-1">
                                        @if($id > 1)
                                            <button class="option-remove-button btn btn-danger"
                                                    type="button" onclick="removeFormRow('form-row-option-{{ $id }}')">
                                                {{ __('Remove') }}
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="form-row" id="form-row-option-1">
                                <div class="form-group col-md-11">
                                    <input class="form-control option-input" type="text" name="option[1]" id="option-1"
                                           data-id="1">
                                </div>
                                <div class="form-group col-md-1"></div>
                            </div>
                        @endif
                    @else
                        @foreach($survey->surveyoption as $option)
                            <div class="form-row" id="form-row-option-{{ $option->id }}">
                                <div class="form-group col-md-11">
                                    <input class="form-control option-input" type="text"
                                           name="option[{{ $option->id }}]" id="option-{{ $option->id }}"
                                           value="{{ $option->option_text }}" data-id="{{ $option->id }}">
                                </div>
                                <div class="form-group col-md-1">
                                    <button class="option-remove-button btn btn-danger" type="button"
                                            onclick="removeFormRow('form-row-option-{{ $option->id }}')">
                                        {{ __('Remove') }}
                                    </button>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @error('option')
                    <span class="invalid-feedback" role="alert" style="display: block;">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <button type="button" id="option-add-button"
                            class="btn btn-primary mb-2">{{ __('Add Option') }}</button>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-primary btn-block">{{ __('Save') }}</button>
                    @if((bool)$newSurvey === false)
                        <button type="button" class="btn btn-info btn-secondary btn-block"
                                onclick="handleShare({{ $survey->id }})">{{ __('Share') }}</button>
                        <button type="button" class="btn btn-danger btn-secondary btn-block"
                                onclick="handleDelete({{ $survey->id }})">{{ __('Delete') }}</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
    @include('partials.deletemodal')
    @include('partials.sharemodal')
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        flatpickr("#expires-at", {
            enableTime: true
        });

        function removeFormRow(idValue) {
            $('#' + idValue.toString()).remove();
        }

        function createFormRow(name, count) {
            var idAttribute = name.toLowerCase() + '-' + count.toString();
            var nameAttribute = name.toLowerCase() + '[' + count.toString() + ']';

            var optionInput = document.createElement('input');
            $optionInput = $(optionInput);
            $optionInput.addClass('form-control')
                .addClass('option-input')
                .attr('type', 'text')
                .attr('name', nameAttribute)
                .attr('id', idAttribute)
                .attr('data-id', count.toString());

            var optionGroup = document.createElement('div');
            $optionGroup = $(optionGroup);
            $optionGroup.addClass('form-group').addClass('col-md-11').append(optionInput);

            var removeButton = document.createElement('button');
            $removeButton = $(removeButton);
            $removeButton.addClass('option-remove-button')
                .addClass('btn')
                .addClass('btn-danger')
                .attr('type', 'button')
                .attr('onclick', 'removeFormRow(\'form-row-' + idAttribute + '\')')
                .text('Remove');

            var removeGroup = document.createElement('div');
            $removeGroup = $(removeGroup);
            $removeGroup.addClass('form-group').addClass('col-md-1').append(removeButton);

            var formRow = document.createElement('div');
            $formRow = $(formRow);
            $formRow.append(optionGroup)
                .append(removeGroup)
                .addClass('form-row')
                .attr('id', 'form-row-' + idAttribute);

            return formRow;
        }

        $('#option-add-button').click(
            function () {
                let optionCount;

                $('.option-input').each(function () {
                    if (!optionCount) {
                        optionCount = this;
                    } else {
                        if (parseInt($(this).data('id')) > parseInt($(optionCount).data('id'))) {
                            optionCount = this;
                        }
                    }
                });

                $('#option-container').append(createFormRow('Option', $(optionCount).data('id') + 1));
            }
        );

        function handleDelete(id) {
            var deleteForm = document.getElementById('deleteSurveyForm');
            deleteForm.action = '/survey/' + id;
            $('#deleteModal').modal('show');
        }

        function handleShare(id) {
            var shareForm = document.getElementById('shareSurveyForm');
            shareForm.action = '/survey/share/' + id;
            $('#shareModal').modal('show');
        }
    </script>
@endsection
