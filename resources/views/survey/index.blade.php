@extends('layouts.app')

@section('content')
    <div class="card card-default">
        <div class="card-header">
            <h3 class="float-left align-middle m-0">{{ __('Surveys') }}</h3>
            <a href="{{ route('survey.create') }}" class="btn btn-success float-right">{{ __('Create Survey') }}</a>
        </div>
        <div class="card-body">
            @include('partials.errors')
            <table class="table">
                <thead>
                <tr class="d-flex">
                    <th class="col-4">{{ __('Name') }}</th>
                    <th class="col-4">{{ __('Expiry Date') }}</th>
                    <th class="col-4">&nbsp;</th>
                </thead>
                </tr>
                <tbody>
                @if($surveys->count() > 0)
                    @foreach($surveys as $survey)
                        <tr class="d-flex">
                            <td class="col-4">
                                <a href="{{ route('survey.show', $survey->id) }}">{{ $survey->name }}</a>
                            </td>
                            <td class="col-4">{{ $survey->expires_at }}</td>
                            <td class="col-4">
                                <div class="float-right">
                                    <a class="btn btn-info btn-sm" style="color: #000000;"
                                       href="{{ route('survey.edit', $survey->id) }}">{{ __('Edit') }}</a>
                                    <button type="button" class="btn btn-danger btn-sm"
                                            onclick="handleDelete({{ $survey->id }})">{{ __('Delete') }}</button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr class="d-flex">
                        <td colspan="3" class="col-12">{{ __('No Surveys found') }}</td>
                    </tr>
                @endif
                </tbody>
            </table>
            @include('partials.deletemodal')
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function handleDelete(id) {
            var deleteForm = document.getElementById('deleteSurveyForm');
            deleteForm.action = '/survey/' + id;
            $('#deleteModal').modal('show');
        }
    </script>
@endsection
