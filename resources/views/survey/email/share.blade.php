@component('mail::message')
{{ $content['body'] }}

@component('mail::button', ['url' => $content['buttonUrl']])
{{ $content['buttonText'] }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
