@extends('layouts.app')

@section('content')
    @include('partials.errors')
    <div class="row">
        <div class="col">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="float-left m-0">{{ $survey->name }}</h3>
                </div>
                <div class="card-body">
                    <div class="card-text">
                        <p>{{ $survey->question }}</p>
                        @if(trim($survey->expires_at) !== '')<p>{{ __('Expires at: ') . $survey->expires_at }}</p>@endif
                    </div>
                    @if((bool)$cookieIsSet === false && (bool)$survey->completed === false)
                        <form action="{{ route('survey.vote', $survey->id) }}" method="post" id="form">
                            @csrf
                            @foreach($survey->surveyoption as $surveyOption)
                                <div class="custom-control custom-radio" style="margin-bottom: .5rem;">
                                    <input type="radio" id="{{ 'option-' . $surveyOption->id }}" name="option"
                                           class="custom-control-input" value="{{ $surveyOption->id }}">
                                    <label class="custom-control-label"
                                           for="{{ 'option-' . $surveyOption->id }}">
                                        {{ $surveyOption->option_text }}
                                    </label>
                                </div>
                            @endforeach
                            <div class="form-group" style="margin-top: .5rem;">
                                <button type="submit"
                                        class="btn btn-success btn-primary btn-block">{{ __('Submit') }}</button>
                            </div>
                        </form>
                    @else
                        @foreach($survey->surveyoption as $surveyOption)
                            @php
                                $voteCount = array_key_exists($surveyOption->id, $votes) ?
                                    $votes[$surveyOption->id]['vote_count'] :
                                    0;
                                $countPercentage = array_key_exists($surveyOption->id, $votes) ?
                                    $votes[$surveyOption->id]['count_percentage'] :
                                    0;
                            @endphp
                            <div>
                                <strong>{{ $surveyOption->option_text }}</strong>
                                <span class="badge badge-primary float-right">
                                    {{ $voteCount . ' ' . ((int)$voteCount === 1 ? __('vote') : __('votes')) }}
                                </span>
                            </div>
                            <div class="progress" style="margin-bottom: 1rem; height: 20px;">
                                <div class="progress-bar bg-info" role="progressbar"
                                     style="color: #000000; width: {{ $countPercentage }}%"
                                     aria-valuenow="{{ $voteCount }}" aria-valuemin="0"
                                     aria-valuemax="{{ $votes['max_vote_count'] }}"></div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
